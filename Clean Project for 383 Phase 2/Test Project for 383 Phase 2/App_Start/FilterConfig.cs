﻿using System.Web;
using System.Web.Mvc;

namespace Test_Project_for_383_Phase_2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
