﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Test_Project_for_383_Phase_2.Models;

namespace Test_Project_for_383_Phase_2.DAL
{
    public class TestContext : DbContext
    {
        public TestContext() : base("TestContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public System.Data.Entity.DbSet<Test_Project_for_383_Phase_2.Models.Sale> Sales { get; set; }
    }
}