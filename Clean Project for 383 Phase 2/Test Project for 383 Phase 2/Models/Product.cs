﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test_Project_for_383_Phase_2.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; } //needs to be unique
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public decimal Price { get; set; }
        public int InventoryCount { get; set; }
    }
}