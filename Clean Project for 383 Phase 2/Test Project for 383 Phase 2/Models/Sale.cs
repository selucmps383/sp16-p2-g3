﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test_Project_for_383_Phase_2.Models
{
    public class Sale
    {
        public int ID { get; set; }
        public string Date { get; set; }
        public decimal TotalAmount { get; set; }
        public string EmailAddress { get; set; }
    }
}