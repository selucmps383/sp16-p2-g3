﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test_Project_for_383_Phase_2.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Email { get; set; } //needs to be unique

        public string Password { get; set; } //needs to be hashed

        public string ApiKey { get; set; }
    }
}