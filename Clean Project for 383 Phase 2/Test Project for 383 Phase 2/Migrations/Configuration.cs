namespace Test_Project_for_383_Phase_2.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Helpers;

    internal sealed class Configuration : DbMigrationsConfiguration<Test_Project_for_383_Phase_2.DAL.TestContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(Test_Project_for_383_Phase_2.DAL.TestContext context)
        {
            context.Users.AddOrUpdate (x => x.Id,
        new User()
        {
            Email = "sa@383.com",
            Password = Crypto.HashPassword("password"),
            ApiKey = "test new" });

            context.Products.AddOrUpdate(x => x.ID,
        new Product()
        {
            Name = "Call of Duty",
            CreatedDate = DateTime.Now,
            LastModifiedDate = DateTime.Now,
            Price = 5999,
            InventoryCount = 50 });
        }
    }
}
