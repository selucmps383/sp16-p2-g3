namespace Test_Project_for_383_Phase_2.Areas.HelpPage.ModelDescriptions
{
    public class EnumValueDescription
    {
        public string Documentation { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}