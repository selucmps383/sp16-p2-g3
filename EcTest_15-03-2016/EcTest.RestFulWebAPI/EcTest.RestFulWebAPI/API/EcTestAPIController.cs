﻿using EcTest.RestFulWebAPI.MailManagement;
using EcTest.RestFulWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcTest.RestFulWebAPI.API
{
    public class EcTestAPIController : ApiController
    {
        DataAccess obj = new DataAccess();
        [HttpGet]
        public List<Product> GetProduct()
        {
            return obj.ProductList.Where(m => m.IsDelete == false).ToList();
        }

        [HttpGet]
        public List<Category> GetCategory()
        {
            return obj.CategoryList.ToList();
        }

        [HttpGet]
        public List<Product> GetProductName(string name, int? id)
        {
            List<Product> list = new List<Product>();
            if (id == null && name == null)
            {
                return list = obj.ProductList.Where(m => m.IsDelete == false).ToList();

            }
            else if (id != null)
            {
                return list = obj.ProductList.Where(m => m.CategoryId == id || m.ProductName.Contains(name) && m.IsDelete == false).ToList();
            }
            else
            {
                return list = obj.ProductList.Where(m => m.ProductName.Contains(name) && m.IsDelete == false).ToList();
            }
        }

        [HttpGet]
        public string Order(string email, string product)
        {
            string[] id = product.Split(',');
            Order order = new Order();
            Order dummyorder = new Order();
            dummyorder.orderitems_fk = new List<OrderItem>();
            order.customer_fk = new Customer();
            int i = 0;
            bool flag = true;
            foreach (var v in id)
            {
                OrderItem orderitem = new OrderItem();
                int pid = Convert.ToInt32(id[i]);
                var p = obj.ProductList.FirstOrDefault(m => m.Id == pid);
                order.customer_fk.EmailId = email;
                order.OrderDate = DateTime.Now.Date;
                orderitem.ProductId = p.Id;
                orderitem.Quantity = 1;
                orderitem.Amount = p.Price * 1;
                orderitem.IsDelete = false;
                orderitem.OrderId = order.Id;
                order.BilledAmount += orderitem.Amount;
                if (flag == true)
                {
                    obj.Save(order.customer_fk);
                    obj.Save(order);
                }
                flag = false;
                orderitem.OrderId = order.Id;
                obj.Save(orderitem);
                dummyorder.orderitems_fk.Add(orderitem);
                i++;
            }
            obj.Save(order);
            MailUtiltity objmail = new MailUtiltity();
            objmail.PurchaseNotify(email, order.Id.ToString(), dummyorder.orderitems_fk);
            return "success";
        }
    }
}
