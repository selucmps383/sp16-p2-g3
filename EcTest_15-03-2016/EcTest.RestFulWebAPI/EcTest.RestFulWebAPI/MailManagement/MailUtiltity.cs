using EcTest.RestFulWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace EcTest.RestFulWebAPI.MailManagement
{
    public class MailUtiltity
    {

        public void PurchaseNotify(string email, string orderid, List<OrderItem> items)
        {
            try
            {
                EmailSettings es = new EmailSettings();
                //Create your WebClient to send the message
                var client = new SmtpClient(es.ServerName, es.ServerPort)         //Ensure that the port is the correct outgoing SMTP port
                {
                    EnableSsl = es.UseSsl,
                    Credentials = new NetworkCredential(es.MailFromAddress, es.Password) //Should this be user123@domain.com / ""?
                    //Have you tried disabling SSL?
                };
                int i = 1;
                decimal total = 0;
                //Define your Message
                MailMessage message = new MailMessage();
                message.From = new MailAddress(es.MailFromAddress, "EcTest");
                message.To.Add(new MailAddress(email));
                message.Subject = "EcTest Shopping";
                message.IsBodyHtml = true;
                StringBuilder body = new StringBuilder();
                body.Append("<html><head><style>tabl{border-collapse: collapse;width:100%;}th,td{text-align:left;padding:8px;}tr:nth-child(even){background-color: #f2f2f2}th{background-color: #4CAF50;color:white;}</style></head><body><div><p>Hi,</p>");
                body.Append("<p> Thank you for purchasing. Your OrderID is #" + orderid + ", our sales team will contact you shourtly.</p></div>");
                body.Append("<table><thead><tr><th>Sr.No</th><th>Items</th><th>Qty</th><th>Price</th></tr></thead><tbody>");
                foreach (var v in items)
                {
                    total += v.product_fk.Price * v.Quantity;
                    body.Append("<tr><td>" + i + "</td>");
                    body.Append("<td>" + @v.product_fk.ProductName + "</td>");
                    body.Append("<td>" + @v.Quantity + "</td>");
                    body.Append("<td>" + @v.product_fk.Price * @v.Quantity + "</td></tr>");
                    i++;
                }
                body.Append("<tr><td>&nbsp;</td><td>&nbsp;</td><td>Total:</td><td>" + total + "</td></tr>");
                body.Append("</tbody></table>");
                //string url = "http://localhost:4958/app/UnsubscribeNewsLetter";

                //body.AppendFormat(url);
                body.Append("<p>Thank you for shopping.</p>");
                body.Append("</body></html>");
                message.Body = body.ToString();

                //Send the actual message
                client.Send(message);
            }
            catch (SmtpFailedRecipientException se)
            {
                using (var errorfile = System.IO.File.CreateText("error-" + DateTime.Now.Ticks + ".txt"))
                {
                    errorfile.WriteLine(se.StackTrace);
                    // variable se is already the right type, so no need to cast it      
                    errorfile.WriteLine(se.FailedRecipient);
                    errorfile.WriteLine(se.ToString());
                }
            }
        }

    }
    public class EmailSettings
    {
        //public string MailFromAddress = "jvsshinde@gmail.com";
        //public bool UseSsl = true;
        //public string Username = "jvsshinde@gmail.com";
        //public string Password = "thesabhaker@$*";
        //public string ServerName = "smtp.gmail.com";
        //public int ServerPort = 587;

        public string MailFromAddress = "jordanrecatto@gmail.com";
        public bool UseSsl = true;
        public string Username = "jordanrecatto@gmail.com";
        public string Password = "";
        public string ServerName = "smtp.gmail.com";
        public int ServerPort = 587;
    }
}