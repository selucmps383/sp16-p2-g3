﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace EcTest.RestFulWebAPI.Models
{
    public class DatabaseContext:DbContext
    {
        public DatabaseContext() : base("constr") { }

        public DbSet<Product> ProductDataSet { get; set; }
        public DbSet<Category> CategoryDataSet { get; set; }
        public DbSet<Order> OrderDataSet { get; set; }
        public DbSet<OrderItem> OrderItemDataSet { get; set; }
        public DbSet<Customer> CustomerDataSet { get; set; }
        public DbSet<Manufacturer> ManufacturerDataSet { get; set; }

    }
}