﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EcTest.RestFulWebAPI.Models
{
    public class DataAccess
    {
        DatabaseContext context = new DatabaseContext();
        public IQueryable<Product> ProductList
        {
            get
            {
                return context.ProductDataSet;
            }
        }

        public IQueryable<Category> CategoryList
        {
            get
            {
                return context.CategoryDataSet;
            }
        }

        public void Save(List<Product> p)
        {
            foreach (var v in p)
            {
                context.Entry(v).State = v.Id == 0 ? EntityState.Added : EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Save(OrderItem orderitems)
        {
            context.Entry(orderitems).State = orderitems.Id == 0 ? EntityState.Added : EntityState.Modified;
            context.SaveChanges();
        }

        public void Save(Order order)
        {

            context.Entry(order).State = order.Id == 0 ? EntityState.Added : EntityState.Modified;
            context.SaveChanges();
        }

        public void Save(Customer customer)
        {

            context.Entry(customer).State = customer.Id == 0 ? EntityState.Added : EntityState.Modified;
            context.SaveChanges();
        }
    }
}