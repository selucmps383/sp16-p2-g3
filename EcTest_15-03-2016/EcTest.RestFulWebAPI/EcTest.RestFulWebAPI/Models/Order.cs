﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EcTest.RestFulWebAPI.Models
{
    [Table("Orders")]
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer customer_fk { get; set; }
        public virtual List<OrderItem> orderitems_fk { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal BilledAmount { get; set; }
        public bool IsDelete { get; set; }
    }
}