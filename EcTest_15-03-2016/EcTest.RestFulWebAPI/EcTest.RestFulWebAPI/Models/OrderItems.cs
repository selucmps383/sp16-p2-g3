﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EcTest.RestFulWebAPI.Models
{
    [Table("OrderItems")]
    public class OrderItem
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order order_fk { get; set; }
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product product_fk { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public bool IsDelete { get; set; }
    }
}