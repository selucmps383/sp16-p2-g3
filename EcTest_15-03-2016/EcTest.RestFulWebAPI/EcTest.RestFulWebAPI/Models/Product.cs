﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EcTest.RestFulWebAPI.Models
{
    [Table("Products")]
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter Product Name")]
        public string ProductName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Image { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        [Required(ErrorMessage = "Enter Price")]
        public decimal Price { get; set; }
        [Required(ErrorMessage = "Enter Stock")]
        public int? InventoryCount { get; set; }
        public int? CategoryId { get; set; }
        public int? ManufacturerId { get; set; }
        public bool? IsDelete { get; set; }
    }
}