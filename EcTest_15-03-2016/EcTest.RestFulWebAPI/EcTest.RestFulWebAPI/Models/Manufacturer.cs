﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EcTest.RestFulWebAPI.Models
{
    [Table("Manufacturers")]
    public class Manufacturer
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string ManufacturerName { get; set; }
        public bool IsDelete { get; set; }
    }
}