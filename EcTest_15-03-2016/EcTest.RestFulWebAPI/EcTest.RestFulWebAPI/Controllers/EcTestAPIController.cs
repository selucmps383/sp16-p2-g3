﻿using EcTest.RestFulWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcTest.RestFulWebAPI.Controllers
{
    public class EcTestAPIController : Controller
    {
        //
        // GET: /EcTestAPI/
        DataAccess obj = new DataAccess();
        public ActionResult GetData()
        {
            var data = obj.GetProduct.ToList();
            return Json(data,JsonRequestBehavior.AllowGet);
        }

    }
}
