﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using CodeFirstModel;
using System.IO;
using EcTest.Filters;





namespace EcTest.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class appController : Controller
    {
        //
        // GET: /app/
        EcTestDataContext context = new EcTestDataContext();
        public ActionResult Index()
        {
            try
            {
                //implementation of IDatabaseInitializer that will delete, recreate,
                //and optionally re-seed the database with data only if the model has changed since the database was created. 
                //This implementation require you to use the type of the Database Context because it’s a generic class. 
                Database.SetInitializer(new DropCreateDatabaseIfModelChanges<EcTestDataContext>());
                ViewBag.dashboard = "active";
                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }


        //Product
        public ActionResult GetProduct()
        {
            List<Product> list = new List<Product>();
            list = context.ProductDataSet.Where(m => m.IsDelete == false).ToList();
            return PartialView("_ProductList", list);
        }
        public ActionResult Product()
        {
            ViewBag.product = "active";
            return View();
        }

        public ActionResult AddProduct()
        {
            TempData["manufacturer"] = new SelectList(context.ManufacturerDataSet.Select(m => new { name = m.ManufacturerName, id = m.Id }), "id", "name");
            TempData["category"] = new SelectList(context.CategoryDataSet.Select(m => new { name = m.CategoryName, id = m.Id }), "id", "name");
            return PartialView("_AddProduct");

        }


        [HttpPost]
        public ActionResult AddProduct(Product product)
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];
                        if (file != null && file.ContentLength > 0)
                        {
                            int MaxContentLenght = 1024 * 1024 * 15;
                            string[] AllowFileExtentions = new string[] { ".png", ".jpg", ".gif", ".bmp" };
                            if (!AllowFileExtentions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                            {
                                TempData["fmsg"] = "Please upload Image(.png, .jpg, .gif, .bmp)";
                                return RedirectToAction("product", "app");
                            }
                            else if (file.ContentLength > MaxContentLenght)
                            {
                                TempData["fmsg"] = "Your image is too large, Maximum allow size is:" + MaxContentLenght + "KB";
                                return RedirectToAction("product", "app");
                            }
                            else
                            {
                                if (System.IO.File.Exists(Server.MapPath("~/ProductImage/") + product.ProductName))
                                {
                                    System.IO.File.Delete(Server.MapPath("~/ProductImage/") + product.ProductName);
                                }
                                var path = Path.Combine(Server.MapPath("~/ProductImage/"), product.ProductName + Path.GetExtension(file.FileName));
                                product.Image = product.ProductName + Path.GetExtension(file.FileName);
                                file.SaveAs(path);
                            }
                        }
                    }
                }
                product.LastModifiedDate = DateTime.Now.Date;
                product.CreateDate = DateTime.Now.Date;
                context.ProductDataSet.Add(product);
                context.SaveChanges();
                TempData["smsg"] = "Product add successfully";
                return RedirectToAction("Product", "app");

            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult DeleteProduct(int id)
        {
            context.DeleteProduct(id);
            return RedirectToAction("product", "app");
        }


        //Manufacturer
        public ActionResult GetManufacturer()
        {
            List<Manufacturer> list = new List<Manufacturer>();
            list = context.ManufacturerDataSet.ToList();
            return PartialView("_ManufacturerList", list);
        }
        public ActionResult Manufacturer()
        {
            ViewBag.manufacturer = "active";
            return View();
        }
        public ActionResult AddManufacturer()
        {
            return PartialView("_AddManufacturer");
        }

        [HttpPost]
        public ActionResult AddManufacturer(Manufacturer manufacturer)
        {
            try
            {
                manufacturer.IsDelete = true;
                context.ManufacturerDataSet.Add(manufacturer);
                context.SaveChanges();
                TempData["smsg"] = "Manufacturer add successfully";
                return RedirectToAction("manufacturer", "app");
            }
            catch (Exception)
            {

                throw;
            }

        }


        //Category
        public ActionResult GetCategory()
        {
            List<Category> list = new List<Category>();
            list = context.CategoryDataSet.ToList();
            return PartialView("_CategoryList", list);
        }
        public ActionResult Category()
        {
            ViewBag.category = "active";
            return View();
        }

        public ActionResult AddCategory()
        {
            return PartialView("_AddCategory");
        }

        [HttpPost]
        public ActionResult AddCategory(Category category)
        {
            try
            {
                category.IsDelete = true;
                context.CategoryDataSet.Add(category);
                context.SaveChanges();
                TempData["smsg"] = "Category add successfully";
                return RedirectToAction("category", "app");
            }
            catch (Exception)
            {

                throw;
            }
        }


        //Reports
        public ActionResult GetOrderList()
        {
            List<Orders> list = new List<Orders>();
            list = context.OrdersDataSet.ToList();
            return PartialView("_OrderList", list);
        }
        public ActionResult Order()
        {
            return View();
        }
    }
}
