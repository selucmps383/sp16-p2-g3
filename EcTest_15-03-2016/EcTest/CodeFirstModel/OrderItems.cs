﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstModel
{
    public class OrderItems
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Orders order_fk { get; set; }
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product product_fk { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public bool IsDelete { get; set; }
    }
}
