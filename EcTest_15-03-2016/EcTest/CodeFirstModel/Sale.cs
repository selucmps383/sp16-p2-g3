﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstModel
{
    public class Sale
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product product_fk { get; set; }
        [Required]
        public decimal TotalAmount { get; set; }
        public string EmailAddress { get; set; }
        public bool IsDelete { get; set; }
    }
}
