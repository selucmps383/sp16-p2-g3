﻿var app = angular.module('sdeal', ['ngRoute', 'controller', 'services']);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/home', {
                templateUrl: 'view/Sdeal.html',
                controller:'homectrl'
            }).
            when('/prodetails', {
                templateUrl: 'view/prodetails.html'
            }).

            otherwise({ redirectTo: '/home' });

    }]);