﻿var app = angular.module('controller', []);
//app.constant("lurl", "http://localhost:8082/API");
app.constant("lurl", "http://localhost:43098/API");

app.controller("homectrl", function ($scope, $http, $location, lurl, Checkout) {

    //$scope.ProductDetails = Proetails.Item();
    //$scope.Cate = Category.Cat();
    $scope.chkout = Checkout.Chkout();
    $scope.carttotal = 0;
    $scope.tempData = {};

    $scope.removeProduct = function (chkout, indx) {
        chkout.splice(indx, 1);
        $scope.getTotal($scope.chkout);
    };

    $scope.add = function (pid, pimg, pname, pprice, pemail) {
        $scope.tempData = {
            pid: pid,
            pimg: pimg,
            pname: pname,
            pprice: pprice
        }
        Checkout.pushData($scope.tempData);
        $scope.getTotal($scope.chkout);
    };

    $scope.getTotal = function (chkout) {
        $scope.carttotal = 0
        angular.forEach(chkout, function (val, key) {
            $scope.carttotal += parseFloat(val.pprice);
        });
    }

    $scope.checkemail = function (email) {
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        if (filter.test(email)) {
            testresults = true
            alert("Success");
        }
        else {
            alert("Please input a valid email address!")
            testresults = false;
        }
        return (testresults)
    }

    $scope.placeOrder = function (chkout, emailid) {
        $scope.email = emailid;
        var myarray = [];
        if ($scope.email != null) {
            // checkemail($scope.email);
            for (var i = 0; i < chkout.length; i++) {
                myarray[i] = chkout[i].pid;
            }
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            $http.get(url + '/EcTestAPI/Order?email=' + $scope.email + '&product=' + myarray.toString(), config)
                .success(function (postdata, status, config) {
                    alert("Your order place successfully!!!")
                    location.reload();
                })
                .error(function (postdata, status) {
                    console.log(postdata);
                    $scope.ServerResponse = "Data: " + postdata +
                        "<hr />status: " + status;
                });

        } else {
            alert("Please enter your email id");
        }
        jQuery('#myModal').modal('hide');
    };

    var url = lurl;
    if ($location.$$host != "localhost")
        url = ourl;
    $http.get(url + '/EcTestAPI/GetProduct').then(function (product) {
        console.log(product);
        $scope.productlist = product.data;
    })

    $http.get(url + '/EcTestAPI/GetCategory').then(function (category) {
        console.log(category);
        $scope.categorylist = category.data;
    })

    $scope.SearchContext = function () {
        $http.get(url + '/EcTestAPI/GetProductName?name=' + $scope.searchString + '&id=' + $scope.categoryId).then(function (product) {
            console.log(product);
            $scope.productlist = product.data;
        })
    }

});