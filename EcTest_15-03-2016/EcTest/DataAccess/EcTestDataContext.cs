﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirstModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DataAccess
{
    
    public class EcTestDataContext : DbContext
    {
        public EcTestDataContext() : base("constr") { }

        public DbSet<Sale> SaleDataSet { get; set; }
        public DbSet<Product> ProductDataSet { get; set; }
        public DbSet<Category> CategoryDataSet { get; set; }
        public DbSet<Manufacturer> ManufacturerDataSet { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Customer> CustomerDataSet { get; set; }
        public DbSet<OrderItems> OrderItemsDataSet { get; set; }
        public DbSet<Orders> OrdersDataSet { get; set; }

        [Table("UserProfile")]
        public class UserProfile
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int UserId { get; set; }
            public string UserName { get; set; }
            public String FullName { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
            public string Role { get; set; }
            public DateTime JoiningDate { get; set; }
            public DateTime LastLogin { get; set; }
            public string ForgetToken { get; set; }
            public bool RememberMe { get; set; }
            public bool IsDelete { get; set; }
        }

        public Product DeleteProduct(int id)
        {
            EcTestDataContext context = new EcTestDataContext();
            var db = context.ProductDataSet.FirstOrDefault(m => m.Id == id);
            if (db != null)
            {
                db.IsDelete = true;
                context.SaveChanges();
            }
            return db;
        }

    }
}
