﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace _383_Phase_2_Group_3.Models
{
    public class Product
        {
            public int ID { get; set; }
            public string Name { get; set; } //needs to be unique
            public DateTime CreatedDate { get; set; }
            public string LastModifiedDate { get; set; }
            public decimal Price { get; set; }
            public int InventoryCount { get; set; }
            public string Category { get; set; }
            public string Manufacturer { get; set; }
        }

    public class ProductDBContext : DbContext
        {
            public DbSet<Product> Product { get; set; }
        }
}