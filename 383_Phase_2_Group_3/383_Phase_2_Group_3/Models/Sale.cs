﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace _383_Phase_2_Group_3.Models
{
    public class Sale
        {
            public int ID { get; set; }
            public string Date { get; set; } 
            public decimal TotalAmount { get; set; }
            public string EmailAddress { get; set; }
        }

    public class SaleDBContext : DbContext
        {
            public DbSet<Sale> Sale { get; set; }
        }
}