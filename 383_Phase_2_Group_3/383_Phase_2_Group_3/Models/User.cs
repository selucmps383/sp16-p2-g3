﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace _383_Phase_2_Group_3.Models
{
    public class User
        {
            public int ID { get; set; }
            public string Email { get; set; } //needs to be unique
            public string Password { get; set; } //needs to be hashed
            public string ApiKey { get; set; }
        }

    public class UserDBContext : DbContext
        {
            public DbSet<User> User { get; set; }
        }
}