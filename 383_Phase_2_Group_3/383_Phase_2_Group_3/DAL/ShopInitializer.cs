﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using _383_Phase_2_Group_3.Models;

namespace _383_Phase_2_Group_3.DAL
{
    public class SchoolInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ShopContext>
    {
        protected override void Seed(ShopContext context)
        {
            var user = new List<User>
            {
            new User{Email="sa@383.com",Password="password",ApiKey=("2005-09-01")},
            };

            user.ForEach(s => context.User.Add(s));
            context.SaveChanges();
        }
    }
}