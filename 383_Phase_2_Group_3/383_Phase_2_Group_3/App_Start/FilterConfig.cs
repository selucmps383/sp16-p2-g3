﻿using System.Web;
using System.Web.Mvc;

namespace _383_Phase_2_Group_3
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
