﻿using System;

namespace StoreApp
{
	public class UserApiKey
	{
		public string ApiKey { get; set; }
		public int UserId { get; set; }
	}
}
