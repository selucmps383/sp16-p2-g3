﻿using System;
using Xamarin.Forms;

namespace StoreApp
{
	public class HomePage : ContentPage
	{
		public HomePage ()
		{

			  
			// provide the heading label
			var headingLabel = new MyLabel {
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				Text = "Welcome",
				FontFamily = Device.OnPlatform (
					"Exo2-ExtraBold",
					"Money Money",
					null),
				FontSize = Device.OnPlatform (54,45,99),
				TextColor = Color.FromHex ("#D84315")

			};



			this.Title = " Dope Store";
			NavigationPage.SetBackButtonTitle (this, "Home");

			var LogInButton = new Button {
				Text = "Log In",
				TextColor = Color.White,
				BackgroundColor = Color.FromHex ("#2196F3"),
				Font = Font.SystemFontOfSize( 20 ),
				WidthRequest = 06,
				HeightRequest = 45,
				

			};
			LogInButton.Clicked += (o, e) => {
			Navigation.PushAsync (new LogInPage ());};
			

			var layout = new StackLayout();
			layout.Children.Add (new BoxView {Color = Color.Transparent, HeightRequest = 50});
			layout.Children.Add (headingLabel);

			layout.Children.Add (new BoxView {Color = Color.Transparent, HeightRequest = 20});
			layout.Children.Add(new BoxView() { Color = Color.FromHex ("EEEEEE"), WidthRequest = 100, HeightRequest = 2 });
			layout.Children.Add (LogInButton);


			// provide the background image
			var homePageImage = new Image {Aspect = Aspect.Fill };

			homePageImage.Source =  Device.OnPlatform(
				ImageSource.FromFile(" "),
				ImageSource.FromFile(" "),
				null);

			// merge views and create a layout
			var relativeLayout = new RelativeLayout ();

			relativeLayout.Children.Add(homePageImage,
				Constraint.RelativeToParent (
					((parent)=>{return 0;})
				));

			relativeLayout.Children.Add(layout,
				Constraint.RelativeToParent((parent) => {return parent.Width/6.5;} ),
				Constraint.RelativeToParent((parent) => {return parent.Height/4;} ));


			Content = relativeLayout;

		}
		 void OnButtonClicked(object sender, EventArgs args)
		{
			Button button = (Button)sender;

			Navigation.PushAsync (new LogInPage (), true);
		} 
	}
}