﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
namespace StoreApp

{

	public class LogInPage: ContentPage
	{
		public LogInPage ()
		{
			var hud = DependencyService.Get<IHud> ();
			this.Title = "Log In";
			Padding = new Thickness (20);


			Label header = new Label {
				Text = "Log In with Your Email",
				FontSize = 27,
				HorizontalOptions = LayoutOptions.Center
			};

			Entry loginInput = new Entry {
				Keyboard = Keyboard.Email,
				Placeholder = "Email Address",
				VerticalOptions = LayoutOptions.Center,

			};

			loginInput.SetBinding (Entry.TextProperty, "Email");

			Entry passwordInput = new Entry {

				Keyboard = Keyboard.Text,
				IsPassword = true, 
				Placeholder = "Password",
				VerticalOptions = LayoutOptions.Center
			};

			loginInput.SetBinding (Entry.TextProperty, "Password");

			var LogInButton = new Button {
				Text = "Log In",
				TextColor = Color.White,
				BackgroundColor = Color.FromHex ("#4FC3F7"),
				Font = Font.SystemFontOfSize (20),
				WidthRequest = 10,
				HeightRequest = 40,


			};


			LogInButton.Clicked += (sender, ea) => {
				new UserRepository ().OnLogInButtonClicked (loginInput.Text, passwordInput.Text);



				if (new UserRepository ().OnLogInButtonClicked (loginInput.Text, passwordInput.Text) == true) {

					Device.StartTimer (new TimeSpan (0, 0, 3), () => {
						hud.ShowSuccessWithStatus("Verified");
						return false; // runs again, or false to stop
					});



					Device.StartTimer (new TimeSpan (0, 0, 8), () => {
						hud.Dismiss ();
						return false; // runs again, or false to stop
					});

				} else {
					hud.ShowErrorWithStatus ("Invalid Credentials");
					DisplayAlert ("Sorry! ", "Please Check your Email or Password!", "Try Again");
				}
			};



			Label noAccount = new Label {
				Text = "Don't have an account, contact administrator?",
				FontSize = 13,
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center
			};
					

			var layout = new StackLayout ();

			layout.Children.Add (new BoxView { Color = Color.Transparent, HeightRequest = 35 });
			layout.Children.Add (header);
			layout.Children.Add (new BoxView { Color = Color.Transparent, HeightRequest = 45 });
			layout.Children.Add (loginInput);
			layout.Children.Add (new BoxView { Color = Color.Transparent, HeightRequest = 5 });
			layout.Children.Add (passwordInput);
			layout.Children.Add (LogInButton);

			ScrollView scroll = new ScrollView {
				Content = layout 

			};

			Content = scroll;
		}
	}
}

